FROM golang
MAINTAINER astaxie xiemengjun@gmail.com

RUN go get github.com/astaxie/beego

RUN go get gitee.com/go-project/beeweb

WORKDIR /go/src/gitee.com/go-project/beeweb

RUN go build

EXPOSE 8080

CMD ["./beeweb"]
